from django.db import models


class ProcessType(models.IntegerChoices):
    LAWSUIT = 0, 'Demanda Judicial'
    REMEDY = 1, 'Recurso'
    REGULATION = 2, 'Regulación'
    REPORT = 3, 'Reporte'

class ProcessStatus(models.IntegerChoices):
    OPENED = 0, 'Abierta'
    PENDING = 1, 'Pendiente de revisión'
    CLOSED = 2, 'Cerrada'


class Process(models.Model):
    title = models.CharField(max_length=127)
    description = models.TextField(max_length=1023, null=True, blank=True)
    process_type = models.IntegerField(choices=ProcessType.choices)
    status = models.IntegerField(
        choices=ProcessStatus.choices,
        default=ProcessStatus.OPENED
    )
    receive_date = models.DateField()
    expiration_date = models.DateField()
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(
        'auth.User',
        on_delete=models.CASCADE,
        related_name='processes'
    )

    class Meta:
        verbose_name_plural = "Processes"
        ordering=('expiration_date',)

    def __str__(self):
        return f'{self.get_process_type_display()} - {self.pk}'


class Note(models.Model):
    comment = models.TextField(max_length=1023)
    created = models.DateTimeField(auto_now_add=True)
    process = models.ForeignKey(
        'processes.Process',
        related_name='notes',
        on_delete=models.CASCADE
    )

class Document(models.Model):
    attachment = models.FileField(upload_to='documents/%Y/%m/%d/')
    created = models.DateTimeField(auto_now_add=True)
    process = models.ForeignKey(
        'processes.Process',
        related_name='documents',
        on_delete=models.CASCADE
    )
