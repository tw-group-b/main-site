from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.urls import reverse


@login_required
def process(request, process_id):
    context = {
        'api_url': reverse('process-detail', kwargs={'pk':process_id}),
        'process_id': process_id
    }
    return render(request, 'processes/details.html', context=context)
