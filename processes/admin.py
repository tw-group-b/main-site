from django.contrib import admin

from processes.models import Document, Note, Process


@admin.register(Document)
class DocumentAdmin(admin.ModelAdmin):
    list_display = ('id', 'attachment', 'created')

@admin.register(Note)
class NoteAdmin(admin.ModelAdmin):
    list_display = ('id', 'comment', 'created')

@admin.register(Process)
class ProcessAdmin(admin.ModelAdmin):
    list_display = ('id', 'process_type', 'title',)
    list_filter = ('process_type',)
    search_fields=('title',)
