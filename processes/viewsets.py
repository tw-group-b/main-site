from rest_framework import mixins, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from processes.models import Process, ProcessType
from processes.serializers import (
    DocumentCreateSerializer,
    NoteCreateSerializer,
    ProcessDetailSerializer,
    ProcessSerializer
)
import logging


class ProcessViewset(mixins.CreateModelMixin,
                     mixins.RetrieveModelMixin,
                     mixins.UpdateModelMixin,
                     mixins.ListModelMixin,
                     viewsets.GenericViewSet):

    queryset = Process.objects.none()
    serializer_class = ProcessSerializer

    def get_queryset(self):
        user = self.request.user
        if user.is_staff:
            queryset = Process.objects.all()
        else:
            queryset = user.processes

        return queryset
    
    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = ProcessDetailSerializer(instance)
        return Response(serializer.data)
    
    @action(detail=False)
    def lawsuits(self, request):
        processes = self.get_queryset().filter(process_type=ProcessType.LAWSUIT)
        serializer = self.get_serializer(processes, many=True)
        return Response(serializer.data)
    
    @action(detail=False)
    def remedies(self, request):
        processes = self.get_queryset().filter(process_type=ProcessType.REMEDY)
        serializer = self.get_serializer(processes, many=True)
        return Response(serializer.data)
    
    @action(detail=False)
    def regulations(self, request):
        processes = self.get_queryset().filter(process_type=ProcessType.REGULATION)
        serializer = self.get_serializer(processes, many=True)
        return Response(serializer.data)
    
    @action(detail=False)
    def reports(self, request):
        processes = self.get_queryset().filter(process_type=ProcessType.REPORT)
        serializer = self.get_serializer(processes, many=True)
        return Response(serializer.data)

    @action(detail=True, methods=['POST'])
    def add_document(self, request, pk=None):
        document_serializer = DocumentCreateSerializer(data=request.data)
        document_serializer.is_valid(raise_exception=True)
        document_serializer.save()
        instance = self.get_object()
        serializer = ProcessDetailSerializer(instance)
        return Response(serializer.data)
    
    @action(detail=True, methods=['POST'])
    def add_note(self, request, pk=None):
        note_serializer = NoteCreateSerializer(data=request.data)
        note_serializer.is_valid(raise_exception=True)
        note_serializer.save()
        instance = self.get_object()
        serializer = ProcessDetailSerializer(instance)
        return Response(serializer.data)
