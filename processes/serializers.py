import os

from rest_framework import serializers

from core.serializers import UserSerializer
from processes.models import Document, Note, Process


class DocumentSerializer(serializers.ModelSerializer):
    filename = serializers.SerializerMethodField()

    class Meta:
        model = Document
        fields = (
            'attachment',
            'created',
            'filename'
        )
    
    def get_filename(self, obj):
        return os.path.basename(obj.attachment.file.name)


class NoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Note
        fields = (
            'id',
            'comment',
            'created'
        )


class ProcessSerializer(serializers.ModelSerializer):
    process_type_display = serializers.SerializerMethodField()
    status_display = serializers.SerializerMethodField()

    class Meta:
        model = Process
        fields = (
            'id',
            'title',
            'description',
            'process_type',
            'process_type_display',
            'receive_date',
            'expiration_date',
            'status',
            'status_display',
            'created_by',
        )
    
    def get_process_type_display(self, obj):
        return obj.get_process_type_display()
    
    def get_status_display(self, obj):
        return obj.get_status_display()
    

class ProcessDetailSerializer(serializers.ModelSerializer):
    process_type_display = serializers.SerializerMethodField()
    status_display = serializers.SerializerMethodField()
    documents = DocumentSerializer(many=True)
    notes = NoteSerializer(many=True)
    created_by = UserSerializer()

    class Meta:
        model = Process
        fields = (
            'id',
            'title',
            'description',
            'process_type',
            'process_type_display',
            'receive_date',
            'expiration_date',
            'notes',
            'documents',
            'status',
            'status_display',
            'created_by',
        )
    
    def get_process_type_display(self, obj):
        return obj.get_process_type_display()
    
    def get_status_display(self, obj):
        return obj.get_status_display()


class DocumentCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Document
        fields = (
            'attachment',
            'process'
        )


class NoteCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Note
        fields = (
            'comment',
            'process',
        )
