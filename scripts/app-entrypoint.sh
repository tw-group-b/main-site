#!/usr/bin/env bash

echo "$0 Migrate"
python manage.py migrate --no-input

echo "$0 Run uwsgi"
exec uwsgi --ini /code/uwsgifile.ini
