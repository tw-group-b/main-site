from django.contrib.auth import logout
from django.shortcuts import redirect, render


def index(request):
    template = 'core/index.html'
    if request.user.is_authenticated:
        template = 'processes/dashboard.html'

    return render(request, template)

def logout_view(request):
    logout(request)
    return redirect('index')
