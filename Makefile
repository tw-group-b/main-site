PROJECT ?= legal_office
COMPOSE_FILE = ./docker/docker-compose.yml
DC=docker-compose -p ${PROJECT} -f ${COMPOSE_FILE}

.PHONY: build
build:
	${DC} build

.PHONY: migrate
migrate:
	${DC} run --rm --entrypoint "python manage.py" app migrate --noinput

.PHONY: makemigrations
makemigrations:
	${DC} run --rm --entrypoint "python manage.py" app makemigrations

.PHONY: server
server:
	${DC} up -d

.PHONY: django-shell
djshell:
	${DC} run --rm --entrypoint "python manage.py" app shell

.PHONY: shell
shell:
	${DC} run --rm --entrypoint "/bin/bash" app

.PHONY: stop
stop:
	${DC} down

.PHONY: clean
clean:
	${DC} down --remove-orphans

.PHONY: clean-volumes
clean-volumes:
	${DC} down --remove-orphans --volumes
