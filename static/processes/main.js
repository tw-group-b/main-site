var app = new Vue({
  el: '#legal-office-app',
  delimiters: ["<%","%>"],
  data: {
    searchText: null,
    searchStatus: null,
    searchType: null,
    processes: [],
  },
  mounted: function() {
    var self = this;
    $.get($('.js-api-url').val(), function(data, status){
      self.$data.processes = data;
    });
  },
  methods: {
    search: function (processes) {
      var self = this;
      var query = self.$data.searchText;
      var status = self.$data.searchStatus;
      var type = self.$data.searchType;

      return processes.filter(function (process) {
        if (!query && !status && !type) {
          return process;
        }
        if (
          (!query || query && process.title.toLowerCase().includes(query.toLowerCase()))
          && (!status || status && status == process.status)
          && (!type || type && type == process.process_type)
        ){
          return process;
        }
      })
    },

    newProcess: function(event) {
      event.preventDefault();
      var self = this;
      var form = $(event.target);
      $.ajax({
        method: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize()
      })
        .done(function(data) {
          $('.js-notifications').append(`
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Creado!</strong> El proceso se ha creado con éxito.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          `);
          self.$data.processes.push(data);
        })
        .fail(function() {
          $('.js-notifications').append(`
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              <strong>Error!</strong> Ha habído un error al crear el proceso.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          `);
        });

      $('#newProcess').modal('hide');
      form.trigger('reset');
    }
  }
})
