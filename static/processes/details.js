var app = new Vue({
  el: '#details-app',
  delimiters: ["<%","%>"],
  data: {
    process: null,
  },
  mounted: function() {
    self = this;
    $.get($('.js-api-url').val(), function(data, status){
      self.$data.process = data;
    });
  },
  methods: {
    newDocument: function(event) {
      event.preventDefault();
      var self = this;
      var form = $(event.target);
      var formData = new FormData(event.target);
      var attachment = form.find(':input[type=file]')[0]
      formData.append(attachment.name, attachment.files[0])

      $.ajax({
        method: form.attr('method'),
        url: form.attr('action'),
        data: formData,
        processData: false,
        contentType: false,
        encType: "multipart/form-data",
      })
        .done(function(data) {
          $('.js-notifications').append(`
          <div class="alert alert-success alert-dismissible fade show" role="alert">
            La acción ha sido completada con éxito.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        `);
          self.$data.process = data;
        })
        .fail(function() {
          $('.js-notifications').append(`
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              <strong>Error!</strong> Ha habído un error al realizar la acción.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          `);
        });

      $('.modal').modal('hide');
      form.trigger("reset");
    },
    ajaxSubmit: function(event) {
      event.preventDefault();
      var self = this;
      var form = $(event.target);
      $.ajax({
        method: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize(),
        headers: {
          "X-CSRFToken": form.find(':input[name=csrfmiddlewaretoken]').val()
        }
      })
        .done(function(data) {
          $('.js-notifications').append(`
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              La acción ha sido completada con éxito.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          `);
          self.$data.process = data;
        })
        .fail(function() {
          $('.js-notifications').append(`
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              <strong>Error!</strong> Ha habído un error al realizar la acción.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          `);
        });

      $('.modal').modal('hide');
      form.trigger('reset');
    }
  }
})
