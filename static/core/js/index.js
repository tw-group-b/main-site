Vue.filter('formatDate', function (value) {
  var date = new Date(value);
  return date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear()
});
